class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.column :title, :string
      t.column :description, :text
      t.column :imege_url, :string
      
      t.timestamps
    end
  end
end
