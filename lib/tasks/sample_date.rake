#require 'faker'
namespace :db do
  #Facer.
  desc "Fill database with sample data"
  task populate: :environment do
    make_users
    make_microposts
    make_relationships
  end
end

def make_users
    
#  task populate: :environment do
    admin=User.create!(name: "1",
                 email: "1@1.ru",
                 password: "123456",
                 password_confirmation: "123456",
                 admin: true)
    User.create!(name: "2",
                 email: "2@2.ru",
                 password: "123456",
                 password_confirmation: "123456",
                 admin: true)
    User.create!(name: "Example User",
                 email: "example@railstutorial.org",
                 password: "foobar",
                 password_confirmation: "foobar",
                 admin: true)
    #puts 'zapis 1 proshla'
       
    
    99.times do |n| 
      # puts 'zapis '+n.to_s+' na4ata'
      name  = Faker::Name.name
      #name=name+n.to_s
      email = "example-#{n+1}@railstutorial.org"
      password  = "123456"
      User.create!(name: name,
                   email: email,
                   password: password,
                   password_confirmation: password)
    end
end

def make_microposts
    users = User.all(limit: 6)
    50.times do
      content = Faker::Lorem.sentence(5)
      users.each { |user| user.microposts.create!(content: content) }
    end
end
    
def make_relationships
  users = User.all
  user  = users.first
  followed_users = users[2..50]
  followers      = users[3..40]
  followed_users.each { |followed| user.follow!(followed) }
  followers.each      { |follower| follower.follow!(user) }
end